package org.hspconsortium.bilirubin.kernicterus;

import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Date;

@Component
public class KernicterusRiskCalculator {

    public enum Risk {
        Critical("Critical"), High("High"), HighIntermediate("High-Intermediate"), LowIntermediate("Low-Intermediate"), Low("Low");

        private String value;

        Risk(String value) {
            this.value = value;
        }


        @Override
        public String toString() {
            return value;
        }

        public Risk parse(String test) {
            for (Risk risk : Risk.values()) {
                if (risk.value.equals(test)) {
                    return risk;
                }
            }
            throw new IllegalArgumentException("Unknown risk value");
        }
    }

    public String toString() {
        return "Kernicterus Risk Calculator";
    }

    public Risk calculateRisk(BigDecimal hoursSinceBirth, BigDecimal serumBilirubin_mg_per_dl) {
        if (serumBilirubin_mg_per_dl.compareTo(criticalRiskLowerLimit(hoursSinceBirth)) >= 0) {
            return Risk.Critical;
        } else if (serumBilirubin_mg_per_dl.compareTo(highRiskLowerLimit(hoursSinceBirth)) >= 0) {
            return Risk.High;
        } else if (serumBilirubin_mg_per_dl.compareTo(highIntermediateLowerLimit(hoursSinceBirth)) >= 0) {
            return Risk.HighIntermediate;
        } else if (serumBilirubin_mg_per_dl.compareTo(lowIntermedicateLowerLimit(hoursSinceBirth)) >= 0) {
            return Risk.LowIntermediate;
        } else {
            return Risk.Low;
        }
    }

    public BigDecimal criticalRiskLowerLimit(BigDecimal hours) {
        return BigDecimal.valueOf(20.0);
    }

    public BigDecimal highRiskLowerLimit(BigDecimal hoursSinceBirthBigDecimal) {
        double hoursSinceBirth = hoursSinceBirthBigDecimal.doubleValue();
        double result = 0.0000000001087380116978890 * Math.pow(hoursSinceBirth, 6)
                - 0.0000000390241213926723000 * Math.pow(hoursSinceBirth, 5)
                + 0.0000051614113948939000000 * Math.pow(hoursSinceBirth, 4)
                - 0.0002969267656958150000000 * Math.pow(hoursSinceBirth, 3)
                + 0.0049045801308693600000000 * Math.pow(hoursSinceBirth, 2)
                + 0.2770830724994080000000000 * hoursSinceBirth + 3.0000000000000000000000000;
        return BigDecimal.valueOf(result);
    }

    public BigDecimal highIntermediateLowerLimit(BigDecimal hoursSinceBirthBigDecimal) {
        double hoursSinceBirth = hoursSinceBirthBigDecimal.doubleValue();
        double result = 0.0000000001117640940944670 * Math.pow(hoursSinceBirth, 6)
                - 0.0000000412521674888165000 * Math.pow(hoursSinceBirth, 5)
                + 0.0000056604841945917500000 * Math.pow(hoursSinceBirth, 4)
                - 0.0003464807831541350000000 * Math.pow(hoursSinceBirth, 3)
                + 0.0075934710390583900000000 * Math.pow(hoursSinceBirth, 2)
                + 0.1810763744197170000000000 * hoursSinceBirth + 2.5000000000000000000000000;
        return BigDecimal.valueOf(result);
    }

    public BigDecimal lowIntermedicateLowerLimit(BigDecimal hoursSinceBirthBigDecimal) {
        double hoursSinceBirth = hoursSinceBirthBigDecimal.doubleValue();
        double result = 0.0000000000055158434836619 * Math.pow(hoursSinceBirth, 6) -
                0.0000000020974548879410700 * Math.pow(hoursSinceBirth, 5) +
                0.0000002627978699654140000 * Math.pow(hoursSinceBirth, 4) -
                0.0000054294662703569000000 * Math.pow(hoursSinceBirth, 3) -
                0.0018169823503626500000000 * Math.pow(hoursSinceBirth, 2) +
                0.2329924996556660000000000 * hoursSinceBirth + 2.0000000000000000000000000;
        return BigDecimal.valueOf(result);
    }

    public BigDecimal hoursMinutesSecondsToHours(int hours, int minutes, int seconds) {
        return BigDecimal.valueOf(
                (double) hours
                        + (((double) (minutes * 100) / 60) / 100)
                        + (((double) (seconds * 100) / 60) / 10000)
        );
    }

    public BigDecimal hoursBetweenDates(Date earlyDate, Date lateDate) {
        if (earlyDate == null || lateDate == null) {
            throw new IllegalArgumentException("Early and late dates are required");
        }

        long millisBetweenDates = lateDate.getTime() - earlyDate.getTime();

        return BigDecimal.valueOf((millisBetweenDates) / 1000.0 / 60.0 / 60.0);
    }

}
