package org.hspconsortium.bilirubin.kernicterus;

import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Date;

public class KernicterusRiskCalculatorTest {

    KernicterusRiskCalculator kernicterusRiskCalculator = new KernicterusRiskCalculator();

    @Test
    public void testToString() {
        Assert.assertEquals("Kernicterus Risk Calculator", kernicterusRiskCalculator.toString());
    }

    @Test
    public void testCalculateRisk_Critical() {
        Assert.assertEquals(KernicterusRiskCalculator.Risk.Critical,
                kernicterusRiskCalculator.calculateRisk(BigDecimal.valueOf(24.0), BigDecimal.valueOf(20.0)));
    }

    @Test
    public void testCalculateRisk_High() {
        Assert.assertEquals(KernicterusRiskCalculator.Risk.High,
                kernicterusRiskCalculator.calculateRisk(BigDecimal.valueOf(24.0), BigDecimal.valueOf(12.5)));
    }

    @Test
    public void testCalculateRisk_HighIntermediate() {
        Assert.assertEquals(KernicterusRiskCalculator.Risk.HighIntermediate,
                kernicterusRiskCalculator.calculateRisk(BigDecimal.valueOf(24.0), BigDecimal.valueOf(9.0)));
    }

    @Test
    public void testCalculateRisk_LowIntermediate() {
        Assert.assertEquals(KernicterusRiskCalculator.Risk.LowIntermediate,
                kernicterusRiskCalculator.calculateRisk(BigDecimal.valueOf(24.0), BigDecimal.valueOf(7.5)));
    }

    @Test
    public void testCalculateRisk_Low() {
        Assert.assertEquals(KernicterusRiskCalculator.Risk.Low,
                kernicterusRiskCalculator.calculateRisk(BigDecimal.valueOf(24.0), BigDecimal.valueOf(5.0)));
    }

    @Test
    public void testHoursMinutesSecondsToHours() throws Exception {
        Assert.assertEquals("1.0", "" + kernicterusRiskCalculator.hoursMinutesSecondsToHours(1, 0, 0));
        Assert.assertEquals("1.0166666666666666", "" + kernicterusRiskCalculator.hoursMinutesSecondsToHours(1, 1, 0));
        Assert.assertEquals("1.0168333333333333", "" + kernicterusRiskCalculator.hoursMinutesSecondsToHours(1, 1, 1));
        Assert.assertEquals("2.0", "" + kernicterusRiskCalculator.hoursMinutesSecondsToHours(2, 0, 0));
        Assert.assertEquals("2.033333333333333", "" + kernicterusRiskCalculator.hoursMinutesSecondsToHours(2, 2, 0));
        Assert.assertEquals("2.0336666666666665", "" + kernicterusRiskCalculator.hoursMinutesSecondsToHours(2, 2, 2));

        Assert.assertEquals("100.505", "" + kernicterusRiskCalculator.hoursMinutesSecondsToHours(100, 30, 30));
    }

    @Test
    public void testCriticalLowerLimit() throws Exception {
        for (int i = 1; i < 120; i++) {
            Assert.assertEquals(BigDecimal.valueOf(20.0), kernicterusRiskCalculator.criticalRiskLowerLimit(BigDecimal.valueOf(i)));
        }
    }

    @Test
    public void testHighRiskLowerLimit() throws Exception {
        Assert.assertEquals(BigDecimal.valueOf(3.281695848360593), kernicterusRiskCalculator.highRiskLowerLimit(BigDecimal.valueOf(1.0)));
        Assert.assertEquals(BigDecimal.valueOf(6.01218241220657), kernicterusRiskCalculator.highRiskLowerLimit(BigDecimal.valueOf(10.0)));
        Assert.assertEquals(BigDecimal.valueOf(8.83598724424452), kernicterusRiskCalculator.highRiskLowerLimit(BigDecimal.valueOf(20.0)));
        Assert.assertEquals(BigDecimal.valueOf(11.021318709527542), kernicterusRiskCalculator.highRiskLowerLimit(BigDecimal.valueOf(30.0)));
        Assert.assertEquals(BigDecimal.valueOf(12.58987214106843), kernicterusRiskCalculator.highRiskLowerLimit(BigDecimal.valueOf(40.0)));
        Assert.assertEquals(BigDecimal.valueOf(13.76257295582322), kernicterusRiskCalculator.highRiskLowerLimit(BigDecimal.valueOf(50.0)));
        Assert.assertEquals(BigDecimal.valueOf(14.765306987457802), kernicterusRiskCalculator.highRiskLowerLimit(BigDecimal.valueOf(60.0)));
        Assert.assertEquals(BigDecimal.valueOf(15.71294218753702), kernicterusRiskCalculator.highRiskLowerLimit(BigDecimal.valueOf(70.0)));
        Assert.assertEquals(BigDecimal.valueOf(16.57164169513624), kernicterusRiskCalculator.highRiskLowerLimit(BigDecimal.valueOf(80.0)));
        Assert.assertEquals(BigDecimal.valueOf(17.199468274875354), kernicterusRiskCalculator.highRiskLowerLimit(BigDecimal.valueOf(90.0)));
        Assert.assertEquals(BigDecimal.valueOf(17.465280123375393), kernicterusRiskCalculator.highRiskLowerLimit(BigDecimal.valueOf(100.0)));
        Assert.assertEquals(BigDecimal.valueOf(17.445918044137535), kernicterusRiskCalculator.highRiskLowerLimit(BigDecimal.valueOf(110.0)));
        Assert.assertEquals(BigDecimal.valueOf(17.701683990844472), kernicterusRiskCalculator.highRiskLowerLimit(BigDecimal.valueOf(120.0)));
    }

    @Test
    public void testHoursBetweenDates() throws Exception {
        Date now = new Date();

        Assert.assertEquals(BigDecimal.valueOf(0.0), kernicterusRiskCalculator.hoursBetweenDates(now, now));

        Date oneHourEarlier = new Date(now.getTime() - (60 * 60 * 1000));
        Assert.assertEquals(BigDecimal.valueOf(1.0), kernicterusRiskCalculator.hoursBetweenDates(oneHourEarlier, now));

        Date fractionHourEarlier = new Date(now.getTime() - (30 * 40 * 123));
        Assert.assertEquals(BigDecimal.valueOf(0.041), kernicterusRiskCalculator.hoursBetweenDates(fractionHourEarlier, now));

        Date fiveDaysEarlier = new Date(now.getTime() - (5 * 24 * 60 * 60 * 1000));
        Assert.assertEquals(BigDecimal.valueOf(120.0), kernicterusRiskCalculator.hoursBetweenDates(fiveDaysEarlier, now));

    }
}